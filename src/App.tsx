import { BrowserRouter, Link, Route, Routes, useLocation } from "react-router-dom"

import { LoginContext, LoginProvider } from "./LoginContext"
import { GlacierContext, GlacierProvider } from "./GlacierContext"

import up_arrow from "./assets/svg/arrowup.svg"
import down_arrow from "./assets/svg/arrowdown.svg"

import logo from "./assets/svg/logo.svg"

import clp from "./assets/images/currency/clp.svg"
import eth from "./assets/images/currency/eth.svg"
import usd from "./assets/images/currency/usdc.svg"
import btc from "./assets/images/currency/btc.svg"

import filter from "./assets/svg/filter-accent.svg"

import "./App.css"
import { ChangeEventHandler, FC, FormEvent, useContext, useEffect, useRef, useState } from "react"

const Modal: FC<{ show: boolean, onClose?: () => void }> = (props): JSX.Element => {
  const [show, setShow] = useState(false)
  const overlay = useRef<HTMLDivElement>(null)

  useEffect(() => {
    setShow(props.show)
    const close = (e: { keyCode: number }) => {
      if (e.keyCode === 27) {
        props.onClose && props.onClose()
      }
    }
    window.addEventListener("keydown", close)
    return () => window.removeEventListener("keydown", close)
  })

  useEffect(() => {
    if (show) {
      overlay.current?.classList.add("visible")
      document.getElementById("body")?.classList.add("overflow-hidden")
    } else {
      document.getElementById("body")?.classList.remove("overflow-hidden")
    }

    return () => document.getElementById("body")?.classList.remove("overflow-hidden")
  }, [show])

  return (
    <>
      {props.show && (
        <div ref={overlay} className="modal-overlay"
          onKeyDown={(e) => { if (e.key == "Escape") setShow(false) }}>
          <div className="modal">
            <div className="pure-g">
              <div className="pure-u-1-1 right">
                <button
                  onClick={() => { props.onClose && props.onClose() }} >
                  <i className="fa-solid fa-xmark" />
                </button>
              </div>
              <div className="pure-u-1-1">
                {props.children}
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  )
}


enum TabsBT {
  DEPOSIT,
  WITHDRAW,
}

const Progress = (props: { value: number }) => {
  return (
    <div className="progress-bar">
      <div className="pb-bg">
        <div className="pb-fg" style={{ width: `${props.value}%` }} />
      </div>
      <small className="label">{props.value} %</small>
    </div>
  )
}

const TokenBalance = (props: { img: string, symbol: string, tab?: TabsBT }) => {

  const [tab, show] = useState<TabsBT | undefined>(props.tab)

  return (
    <>
      <div className="pure-g container token-balance">
        <div className="pure-u-1-5 logo-symbol">
          <img src={props.img} />
          <div>{props.symbol}</div>
        </div>

        <div className="amount pure-u-1-5">
          <div>1.000.000</div>
          <small>$1.000.000</small>
        </div>

        <div className="pure-u-1-5">
          <Progress value={75} />
        </div>

        <div className="actions pure-u-2-5">
          <button className="btn btn-default" onClick={() => show(TabsBT.DEPOSIT)} >Retirar</button>
          <button className="btn btn-primary" onClick={() => show(TabsBT.WITHDRAW)}>Depositar</button>
        </div>
      </div>
      {tab && (
        <Modal show={true} onClose={() => show(undefined)}>
          <BankTransactions tab={tab} />
        </Modal>
      )
      }
    </>
  )
}

const Totals = () => {
  return (
    <div className="pure-g total">

      <div className="pure-u-1-1 container box-total">

        <div className="pure-u-1-1">
          <label htmlFor="total-amount-native" className="total-title">
            Balance&nbsp;
            <button><i className="fa-solid fa-eye-slash" /> </button>
          </label>
        </div>
        <div id="pure-u-1-1 total-amount-native" className="pure-u-1-1 token-amount">$ 1.000.000</div>

      </div>

      <div id="in-out" className="pure-u-1-1 box-inout">

        <div className="pure-g container">

          <div className="pure-u-1-1">
            <div id="in" className="pure-u-1-2">
              <label htmlFor="in-amount">GCLP Entrante <img src={up_arrow} /></label>
              <div className="amount">
                <div>$ 1.500.000</div>
                <small>$ 200</small>
              </div>
            </div>
            <div className="pure-u-1-2" id="out">
              <label htmlFor="out-amount">GCLP Saliente <img src={down_arrow} /></label>
              <div className="amount" >
                <div>$ 1.500.000</div>
                <small>$ 200</small>
              </div>
            </div>
          </div>

        </div>
      </div>

    </div >
  )
}

const Transactions = () => {
  return (
    <div className="pure-g pure-u-1-1">
      <div className="pure-u-1-1">
        <h1 className="title-component">Transacciones</h1>
      </div>
      <div className="pure-g container">
        <div className="pure-u-1-1 filter-bar">
          <button className="btn btn-filter">
            <img src={filter} alt="" />
            <div className="btn-filter-legend">Filtrar por</div>
          </button>
        </div>
        <div className="pure-u-1-1">
          <div className="pure-g table-head">
            <div className="type pure-u-1-5">
              <div className="col-head">
                Transacciones
                <button className="btn-sort">&nbsp;</button>
              </div>
            </div>
            <div className="pure-u-1-5">
              <div className="col-head amount right">
                Monto
                <button className="btn-sort">&nbsp;</button>
              </div>
            </div>
            <div className="pure-u-1-5">
              <div className="col-head token right">
                Activo
                <button className="btn-sort">&nbsp;</button>
              </div>
            </div>
            <div className="pure-u-1-5">
              <div className="col-head state right">
                Estado
                <button className="btn-sort">&nbsp;</button>
              </div>
            </div>
            <div className="pure-u-1-5" >
              <div className="col-head date right">
                Fecha
                <button className="btn-sort">&nbsp;</button>
              </div>
            </div>
          </div>
          <div className="pure-g table-body">
            <div className="pure-u-1-1 table-row">
              <div className="pure-u-1-5 table-col type">Depósito</div>
              <div className="pure-u-1-5 table-col right amount">1.000.000</div>
              <div className="pure-u-1-5 table-col right token">CLP</div>
              <div className="pure-u-1-5 table-col right state">
                Confirmado
                <a
                  href="https://mumbai.polygonscan.com/tx/0x26bdb42417a961899fec12ac22ddec6baef5731824ef8b8ab7f4bec1aefe0bcc"
                  target="_blank"
                  className="btn-link-scanner">*
                </a>
              </div>
              <div className="pure-u-1-5 table-col right date">
                Hace unos segundos...
              </div>
            </div>
          </div>
          <div className="pure-g table-footer">
            <div className="pure-u-1-1 paginator">
              <div className="pure-u-1-3 page-descr">
                10 transacciones de un total de 20
              </div>
              <div className="pure-u-1-3 pages">
                <div className="page-numbers">
                  <button className="paginator-link link-previous">&nbsp;</button>
                  <button className="paginator-link ">1</button>
                  <button className="paginator-link active">2</button>
                  <button className="paginator-link ">3</button>
                  <button className="paginator-link link-next">&nbsp;</button>
                </div>
              </div>
              <div className="pure-u-1-3 gotopage">
                <div className="pure-u-1-1">
                  <label htmlFor="gotopage">Ir a la Página</label>
                  <input type="text" name="gotopage" placeholder="0" />
                  <button className="btn btn-small">Ir</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

const Lend = () => {
  return (
    <>
    </>
  )
}

const Wallet = () => {
  return (
    <>
      <div className="pure-g balances">
        <div className="pure-u-1-4" >
          <Totals />
        </div>
        <div className="pure-u-3-4">
          <div className="pure-g token-balances">
            <div className="pure-u-1-1">
              <TokenBalance symbol="CLP" img={clp} />
            </div>
            <div className="pure-u-1-1">
              <TokenBalance symbol="ETH" img={eth} />
            </div>
            <div className="pure-u-1-1">
              <TokenBalance symbol="USD" img={usd} />
            </div>
            <div className="pure-u-1-1">
              <TokenBalance symbol="BTC" img={btc} />
            </div>
          </div>
        </div>
      </div>
      <div id="transactions" className="pure-u-1-1">
        <Transactions />
      </div>
    </>
  )
}

const Header = () => {
  const location = useLocation()

  const { email, loading, logged } = useContext(GlacierContext)
  const { login_loading, logout } = useContext(LoginContext)

  const rootStyle = location.pathname == "/" ? "active" : ""
  const lendStyle = location.pathname == "/lend" ? "active" : ""
  const borrowStyle = location.pathname == "/borrow" ? "active" : ""

  const [showLogin, setShowLogin] = useState(false)
  const [msg, setMsg] = useState("Iniciar sesión")

  useEffect(() => {
    if (login_loading) return
    if (email) setMsg(email)
    else setMsg("Iniciar sesión")
  }, [email, login_loading])

  return (
    <>
      <div className="pure-g header">
        <div className="pure-u-4-24">
          <img className="logo" src={logo} />
        </div>
        <div className="pure-u-10-24">
          <Link className={rootStyle} to="/">Mi Billetera</Link>
          <Link className={lendStyle} to="/lend">Prestar</Link>
          <Link className={borrowStyle} to="/borrow">Pedir Prestado</Link>
        </div>
        <div className="pure-u-10-24 right">
          <button
            onClick={() => {
              if (!logged) setShowLogin(true)
              else {
                setMsg("Saliendo...")
                logout()
                  .then(() => setMsg("Iniciar sesión"))
              }
            }}
            className={`btn btn-primary btn-login ${login_loading ? "loading" : ""}`}
            disabled={login_loading}
          >
            {msg}
          </button>
          
        </div>
      </div>
      <Modal show={showLogin} onClose={() => setShowLogin(false)}>
        <LoginForm onDone={() => setShowLogin(false)} />
      </Modal>
    </>
  )
}

const Input = (
  props: {
    onChange?: (value: string | undefined) => void,
    onInvalid?: ChangeEventHandler<HTMLInputElement>,
    className?: string,
    disabled?: boolean,
    type?: string
  }
) => {
  const [style, setStyle] = useState("")

  const input = useRef<HTMLInputElement>(null)

  const checkValidity = () => {

    if (input.current?.validity.valid) props.onChange && props.onChange(input.current.value)
    else props.onChange && props.onChange(undefined)

    if (
      (input.current?.validity.valid && input.current.value != "") ||
      (!input.current?.validity.valid && !input.current?.validity.valueMissing)
    ) {
      setStyle("is-valid")
    } else {
      setStyle("")
    }
  }

  useEffect(() => input.current?.focus())

  return (
    <div className="input-container">
      <input
        disabled={props.disabled}
        tabIndex={0}
        ref={input}
        onInvalid={props.onInvalid}
        onInput={() => checkValidity()}
        onChange={(i) => i.target.validity.valid && props.onChange && props.onChange(i.target.value)}
        id="amount"
        type={props.type}
        className={style + " " + props.className}
      />
      <label htmlFor="amount">Correo electrónico</label>
    </div>
  )
}


const LoginForm = (props: { onDone?: (email: string) => void }) => {

  const { loading, signin } = useContext(GlacierContext)

  const [email, setEmail] = useState<string | undefined>(undefined)
  const [message, setMessage] = useState<string>("Entrar")
  const [disabled] = useState(false)

  const login = (e: FormEvent<HTMLFormElement>) => {
    if (!email) return
    setMessage("Ingresando...")
    signin(email)
      .then(() => {
        setMessage("Entrar")
        props.onDone && props.onDone(email)
      })
    e.preventDefault()
  }

  return (
    <>
      <div className="pure-g tabs">
        <div className="pure-u-1-1">
          <button className="tab-header-link no-bottom light">Ingreso</button>
        </div>
        <div className="pure-u-1-1 tab-body alt">
          <form onSubmit={(e) => login(e)}>
            <Input
              disabled={disabled || loading}
              type="email" onChange={(value) => { setEmail(value) }}
            />
            <div className="tab-buttons">
              <button
                className={`btn btn-primary full-width ${loading && "loading"}`}
                disabled={!email}
              >
                {message}
              </button>
            </div>
          </form>
        </div>
      </div>
    </>
  )
}

type BankTransaction = {
  bank_account_id: string,
  amount: string,
}

type Deposit = {
  amount: string,
  voucher?: string,
}

type Withdraw = {
  bank_account_id: string,
  amount: string,
}


export const BankTransactions = (
  props: {
    tab?: TabsBT,
    onTransaction?: (trx: Deposit | Withdraw) => void,
  }) => {

  const [tab, setTab] = useState(props.tab || TabsBT.DEPOSIT)

  console.log(">", tab)
  return (
    <>
      <div className="pure-g tabs">
        <div className="pure-u-1-2">
          <button
            className={`tab-header-link ${tab == TabsBT.DEPOSIT ? "active" : ""}`}
            onClick={() => setTab(TabsBT.DEPOSIT)}>
            Depositar
          </button>
        </div>
        <div className="pure-u-1-2">
          <button
            className={`tab-header-link ${tab == TabsBT.WITHDRAW ? "active" : ""}`}
            onClick={() => setTab(TabsBT.WITHDRAW)}>
            Retirar
          </button>
        </div>
        <div className="pure-u">
          {tab == TabsBT.DEPOSIT && (
            <div>a</div>
          )}
          {tab == TabsBT.WITHDRAW && (
            <><input type="text" /></>
          )}
        </div>
      </div>
    </>
  )
}

const App = () => {
  // const [showModal, setShowModal] = useState(true)

  useEffect(() => {
    //    setTimeout(() => setShow(true), 0)
  }, [])

  return (
    <BrowserRouter>
      <LoginProvider>
        <GlacierProvider>
          <div className="pure-g main">
            <div className="pure-u-1-1">
              <Header />
            </div>
            <div className="pure-u-1-1">
              <Routes>
                <Route path="/" element={<Wallet />} />
                <Route path="/lend" element={(<Lend />)} />
                <Route path="/borrow" element={<Lend />} />
              </Routes>
            </div>
          </div>
        </GlacierProvider>
      </LoginProvider>
    </BrowserRouter>
  )
}

export default App

