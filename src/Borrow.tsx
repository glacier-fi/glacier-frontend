import { useState } from "react"
import clp from "./assets/images/currency/clp.svg"
import { Button } from "./Button"
import { Input } from "./Input"
import { Modal } from "./Modal"
import { Progress } from "./Progress"

enum Show {
  None = 0,
  Wallet,
  Lend,
}

export const Borrow = () => {

  return (
    <>
      <div className="container-fluid mb-lg-4">
        <div className="row">
          <div className="col-12 col-lg-4">
            <div className="card-glacier card-borrow">
              <div className="card-header">
                <h1 className="title-box">Tasa de Interés</h1>
                <h2 className="label-box">Actual</h2>
                <div className="value-box">15.6</div>
              </div>
              <div className="card-body">
                <ul className="data-list-box">
                  <li>
                    <em>Utilización de la reserva :</em>
                    <Progress value={53} />
                  </li>
                  <li>
                    <em>Total prestado :</em> <span>$202.320.378</span>
                  </li>
                  <li>
                    <em>Liquidez Disponible :</em> <span>$155.556.489</span>
                  </li>
                  <li>
                    <em>Tamaño de la reserva :</em> <span>$358.089.166</span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="col-12 col-lg-4">
            <div className="card-glacier card-borrow">
              <div className="card-header">
                <h1 className="title-box">Pedir Prestado</h1>
                <h2 className="label-box">Has Pedido</h2>
                <div className="value-box">
                  $700.000,23 <small>4201</small>
                </div>
                <div className="button-bar">
                  <button className="btn btn-default">Pagar</button>
                </div>
              </div>
              <div className="card-body">
                <ul className="data-list-box">
                  <li>
                    <em>Utilización de línea de crédito :</em>
                    <Progress value={86} />
                  </li>
                  <li>
                    <em>Línea de crédito :</em> <span>$202.320.378</span>
                  </li>
                  <li>
                    <em>Tu Colateral :</em> <span>$155.556.489</span>
                  </li>
                </ul>
                <div className="button-bar">
                  <button className="btn btn-primary">Pedir</button>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-lg-4">
            <div className="card-glacier card-borrow">
              <div className="card-header">
                <h1 className="title-box">Factor de Salud</h1>
                <h2 className="label-box">Tu Porcentaje de Salud</h2>
                <div className="value-box">
                  1.6<small>%</small>
                </div>
              </div>
              <div className="card-body">
                <ul className="data-list-box">
                  <li>
                    <em>Umbral de desviación :</em>
                        
                    <span>0.5%</span>
                  </li>
                  <li>
                    <em>Tiempo prox. actual. :</em> <span>00:53:27</span>
                  </li>
                  <li>
                    <em>Última actualización :</em> <span>5 minutos</span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
