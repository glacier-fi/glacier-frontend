import { FC } from "react"

export const Button: FC<{
  className?: string
  loading?: boolean
  disabled?: boolean
  onClick?: () => void
}> = (props) => {
  let style = props.className || ""

  style = style + " " + (props.loading ? "loading " : "")
  style = style + " " + (props.disabled ? "btn-disabled" : "")

  return (
    <button className={`btn ${style}`} onClick={props.onClick}>
      {props.children}
    </button>
  )
}

