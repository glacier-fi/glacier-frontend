import { BigNumber, FixedNumber } from "ethers"
import React, {
  ChangeEvent,
  useContext,
  useEffect,
  useRef,
  useState,
} from "react"
import { Button } from "./Button"
import { GlacierContext, GlacierTransaction } from "./GlacierContext"
import { Input } from "./Input"

export const DepositGCLP = (props: { onCreate?: (d: GlacierTransaction) => void }) => {

  const { tokens, balances, native_token, submitDeposit } = useContext(GlacierContext)

  const [max_deposit] = useState(1000000)
  const [min_deposit] = useState(1)
  const [amount, setAmount] = useState<BigNumber | undefined>(BigNumber.from("100000000000000"))
  const [deposited, setDeposited] = useState("0")
  const [file_name, setFileName] = useState<string | undefined>(undefined)
  const [voucher, setVoucher] = useState<string | undefined>(undefined)
  const fileRef = React.useRef<HTMLInputElement>(null)

  const changeAmount = (a?: string) => {
    if (!a) {
      setAmount(undefined)
      return
    }

    setAmount(BigNumber.from(a).mul(10 ** 8))
  }

  useEffect(() => {
    const balance = balances.find(b => b.factory.tokenAddress == native_token)
    const token = tokens.find(t => t.id == balance?.factory.tokenAddress)

    if (!token || !balance) return

    const format = new Intl.NumberFormat("es", token.format)
    const deposited_bn = BigNumber.from(balance.balance).add(BigNumber.from(balance.inTransitIn))

    setDeposited(format.format(FixedNumber.fromValue(deposited_bn, token.decimals).toUnsafeFloat()))

  }, [balances])

  const fileChanged = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.files) {
      const reader = new FileReader()
      setFileName(e.target.files[0].name)
      reader.readAsDataURL(e.target.files[0])
      reader.onload = () => {
        setVoucher(reader.result as string)
      }
    }
  }

  const [gracePeriod, setGracePeriod] = useState(false)
  const [sending, setSending] = useState(false)
  const [amount_id] = useState(0)

  const timer = useRef(-1)
  const countdown_interval = useRef(-1)

  const cancelDeposit = () => {
    window.clearTimeout(timer.current)
    window.clearInterval(countdown_interval.current)
    setSecs(5)
    setGracePeriod(false)
  }

  const [secs, setSecs] = useState(5)

  const sendDeposit = () => {
    if (!voucher || !amount) return
    setGracePeriod(true)

    let countdown = 5
    countdown_interval.current = window.setInterval(() => setSecs(--countdown), 1000)

    timer.current = window.setTimeout(() => {
      setGracePeriod(false)
      setSending(true)
      submitDeposit({
        voucher: voucher,
        amount: BigNumber.from(amount).toString(),
      })
        .then(d => {
          window.clearTimeout(timer.current)
          window.clearInterval(countdown_interval.current)
          props.onCreate && props.onCreate(d)
        })
        .catch((err) => {
          console.log(err)
          window.clearTimeout(timer.current)
          window.clearInterval(countdown_interval.current)
          setSending(false)
          setGracePeriod(false)
        })
    }, 5010)

    return () => {
      window.clearInterval(timer.current)
      window.clearInterval(countdown_interval.current)
    }
  }

  const formatAmount = () => {
    const fmt = new Intl.NumberFormat("es", { maximumFractionDigits: 0 })
    if (amount)
      return fmt.format(FixedNumber.fromValue(amount, 8).toUnsafeFloat())
    else
      return ""
  }

  return (
    <>
      {!gracePeriod && !sending && (
        <div className="tab-form">
          <div className="tab-data">
            <ul>
              <li>
                <span className="legend"> Banco : </span>
                <span className="val">Santander</span>
              </li>
              <li>
                <span className="legend">Cuenta corriente :</span>
                <span className="val">1231238231238-1</span>
              </li>
              <li>
                <span className="legend">Titular :</span>
                <span className="val">Glacier SPA</span>
              </li>
              <li>
                <span className="legend">Rut :</span>
                <span className="val">76.092.963-8</span>
              </li>
              <li>
                <span className="legend">Correo :</span>
                <span className="val">gx@glacier.fi</span>
              </li>
            </ul>
          </div>
          <Input label="Monto"
            key={amount_id}
            type="number"
            step={1}
            max={max_deposit}
            min={min_deposit}
            disabled={sending || gracePeriod}
            onChange={a => changeAmount(a)} />
          <div className="input-file">
            <i className="icon-input-file fa-light fa-paperclip-vertical" />
            <span className="label-input-file ">{file_name ? file_name : "Seleccionar comprobante..."}</span>
            <input
              ref={fileRef}
              disabled={sending || gracePeriod}
              onChange={fileChanged} type="file" id="voucher" className="btn-icon-text--voucher"
            />
          </div>
          <hr />
          <div className="tab-data">
            <h2 className="val">
              En los últimos 30 días</h2>
            <ul>
              <li>
                <span className="legend"> Has depositado :</span>
                <span className="val"> $ {deposited}</span>
              </li>
              <li>
                <span className="legend"> Máximo :</span>
                <span className="val"> $ 1.000.000</span>
              </li>
            </ul>
          </div>
        </div>
      )}
      {(gracePeriod || sending) && (
        <div className="tab-form">
          <div className="tab-title">
            NOTIFICASTE
          </div>
          <div className="tab-amount">
            {formatAmount()}
          </div>
          <div className="tab-check">
            <i className="fa-solid fa-check primary" />
            <small>La cuenta bancaria debe estar asociada a tu RUT.</small>
          </div>
          <div className="tab-check">
            <i className="fa-solid fa-check primary" />
            <small>La validación será realizada dentro de las próximas 24 horas.</small>
          </div>
        </div>
      )}
      <div>
        {!sending && !gracePeriod && (
          <Button
            className={`btn btn-full 
            ${(!voucher || !amount) ? "btn-disabled" : "btn-primary"}
          `}
            onClick={() => sendDeposit()}
            disabled={!voucher || !amount}>Notificar</Button>
        )}
        {sending && !gracePeriod && (
          <Button
            className="btn btn-full btn-disabled loading">
            Enviando...
          </Button>
        )}
        {!sending && gracePeriod && (
          <Button
            className="btn btn-full btn-cancel"
            onClick={() => cancelDeposit()}
          >
            Cancelar... {secs}
          </Button>
        )}
      </div>

    </>
  )
}

