import Axios from "axios"
import { createContext, FC, useContext, useEffect, useRef, useState } from "react"
import { LoginContext } from "./LoginContext"
import config from "./config"
import { createClient } from "urql"
import { genQueryUpdate } from "./GraphQuery"
import { Modal } from "./Modal"

export type UserInfo = {
  id: string
  kycInfo: { email: string },
  addresses: { [key: string]: string },
}

export type UserIdRequest = {
  id: string
}

export type NewAddressResponse = {
  address: string
}

export type DepositInfo = {
  voucher: string;
  amount: string;
};

export interface BankAccountForm {
  bankId: string;
  account: string;
  accountType: string;
}

export type WithdrawInfo = {
  bankAccountId: string;
  amount: string;
};

export type Token = {
  id: string
  name: string
  symbol: string
  decimals: number
  ratio?: string
  format: {
    maximumFractionDigits?: number
    minimumFractionDigits?: number
  }
}

export type GlacierTransaction = {
  id: string,
  type: string,
  hash: string,
  amount: string,
  createdAt: number,
}

export type BankAccount = {
  id: string,
  account: string,
  bankId: string,
}

export type Request = {
  amount: string,
  lastUpdate: number,
  status: string,
  type: string,
  token?: string,
  factory: {
    tokenAddress: string
  },
  transactions:
  {
    "id": string,
  }[]
}

export type Balance = {
  factory: { tokenAddress: string },
  balance: string,
  inTransitIn: string,
  inTransitOut: string,
}

export type Rate = {
  pair: { id: string },
  close: string,
}

export interface IGlacierContext {
  rpc?: string,
  loading: boolean,
  logged: boolean,
  email: string | undefined,
  signin: (email: string) => Promise<string>,
  logout: () => Promise<void>

  user_info?: UserInfo,
  native_token: string,
  tokens: Token[],
  balances: Balance[],
  rates: Rate[],
  bank_accounts: BankAccount[],
  btc_rate?: string,
  requests: Request[],
  pending_requests: Request[],
  deposit_address?: string,

  aave_status?: AAVEResponse,

  submitDeposit: (deposit_info: DepositInfo) => Promise<GlacierTransaction>,
  submitWithdraw: (withdraw_info: WithdrawInfo) => Promise<GlacierTransaction>,

  submitBankAccount: (bank_account_form: BankAccountForm) => Promise<BankAccount>,
  getNewAddress: (response: UserIdRequest) => Promise<NewAddressResponse>,
}

type AAVEResponse = {
  GETH: {
    result: { hex: string }[]
  },
  GCLP: {
    result: { hex: string }[]
  },
  user: {
    GETH: {
      result: { hex: string }[]
    },
    GCLP: {
      result: { hex: string }[]
    },
  }
}

const createAxios = async (token: string) => {
  return Axios.create({
    headers: { Authorization: "Bearer " + token },
  })
}




export const GlacierContext =
  createContext<IGlacierContext>(
    {
      logged: false,
      email: "",
      loading: false,
      signin: async () => "",
      logout: async () => { return },

      native_token: config.native_token,
      bank_accounts: [],
      balances: [],
      tokens: [],
      rates: [],
      requests: [],
      pending_requests: [],

      submitDeposit: () => { throw "Not implemented" },
      submitWithdraw: () => { throw "Not implemented" },

      submitBankAccount: () => { throw "Not implemented" },
      getNewAddress: () => { throw "Not implemented" },
    }
  )


export const now = () => (new Date()).getTime()

const client = createClient({
  url: config.endpoints.thegraph
})

export const GlacierProvider: FC = ({ children }) => {

  const { signin, login_loading, logout, logged, email, getAuthToken } = useContext(LoginContext)
  const [loading, setLoading] = useState(true)

  const [native_token] = useState(config.native_token)
  const [user_info, setUserInfo] = useState<UserInfo | undefined>(undefined)
  const [bank_accounts, setBankAccounts] = useState<BankAccount[]>([])
  const [deposit_address, setDepositAddress] = useState<string | undefined>(undefined)
  const [aave_status, setAAVEStatus] = useState<AAVEResponse | undefined>(undefined)

  useEffect(() => {
    if (!logged) {
      setBalances([])
      setRequests([])
      setDepositAddress(undefined)
      setCustomerAddress("")
      setLoading(false)
      return
    }

    setLoading(true)

    getAuthToken()
      .then(t => {
        if (t)
          return createAxios(t)
      })
      .then(client => {
        if (client)
          return client.post(config.endpoints.signin)
      })
      .then(resp => {
        setUserInfo(resp?.data.userInfo)
        setBankAccounts(resp?.data.bankAccounts)
        setDepositAddress(resp?.data.depositAddress)
        setCustomerAddress(resp?.data.userInfo.addresses["mumbai"])
      })
      .catch(e => console.error(e))
  }, [logged])

  const [customer_address, setCustomerAddress] = useState("")

  const [tokens] = useState(config.tokens)
  const [balances, setBalances] = useState<Balance[]>([])
  const [rates, setRates] = useState<Rate[]>([])
  const [btc_rate] = useState<string>(config.btc_ratio)

  const [last_updated, setLastUpdated] = useState(0)

  const [requests, setRequests] = useState<Request[]>([])
  const [pending_requests, setPendingRequests] = useState<Request[]>([])

  const timer = useRef(-1)

  useEffect(() => {

    if (!logged) {
      setLoading(false)
      return
    }

    let time = 30000

    if (!customer_address || customer_address == "") return

    if (last_updated == 0) {
      setLoading(true)
      time = 0
    }

    const update = () => {
      client.query(
        genQueryUpdate(customer_address, last_updated)
      )
        .toPromise()
        .then(response => {
          if ((response.data.requests as Request[]).length > 0) {
            const r = response.data.requests as Request[]

            setPendingRequests([])

            const lu = r.reduce((acc, r) => {
              return Math.max(r.lastUpdate, acc)
            }, 0)

            if (lu == 0) setLastUpdated(last_updated + 1)
            else setLastUpdated(lu)

            setRequests([...r, ...requests])
            setBalances(response.data.balances as Balance[])
            setRates(response.data.priceCandles as Rate[])
          } else {
            setLastUpdated(last_updated + 1)
          }
          setLoading(false)
        })

      getAuthToken()
        .then((t) => {
          if (!t) {
            console.error("cant get token")
            return
          }
          return createAxios(t)
            .then(client => {
              if (!client) {
                console.error("cant get client (axios)")
                return
              }

              return client
                .get(config.endpoints.aave)
                .then((response) => {
                  setLoading(false)
                  setAAVEStatus(response.data as AAVEResponse)
                  console.log(response.data)
                })
                .catch((e) => {
                  setLoading(false)
                })
            })
            .catch((err) => {
              console.error(err)
              setLoading(false)
            })
        })
    }
    if (last_updated == 0) update()
    if (last_updated > 0) timer.current = window.setTimeout(update, time)

    return () => window.clearTimeout(timer.current)

  }, [last_updated, customer_address, logged])

  useEffect(() => {
    if (!login_loading && !logged) {
      setLoading(false)
    } else {
      setLoading(loading || login_loading)
    }
  }, [login_loading, logged])

  const submit =
    <T, R>(endpoint: string, errMsg: string, callback: (data: R) => void) =>
      (e: T): Promise<R> => {
        setLoading(true)
        return new Promise((res, rej) => {
          getAuthToken()
            .then((t) => {
              if (!t) {
                rej("Error : cant get auth token for submit")
                return
              }
              return createAxios(t)
                .then(client => {
                  if (!client) {
                    rej("cant get client (axios)")
                    return
                  }

                  return client
                    .post(endpoint, e)
                    .then((response) => {
                      setLoading(false)
                      callback(response.data)
                      setLastUpdated(last_updated + 1)
                      res(response.data)
                    })
                    .catch((e) => {
                      setLoading(false)
                      rej(e)
                    })
                })
                .catch((err) => {
                  setLoading(false)
                  rej(err)
                })
            })
        })
      }


  const getNewAddress = submit<UserIdRequest, NewAddressResponse>(
    config.endpoints.off_chain_contract,
    "error obteniendo nueva dirección de depósito",
    (addrr) => {
      setDepositAddress(addrr.address)
    }
  )

  const submitDeposit = submit<DepositInfo, GlacierTransaction>(
    config.endpoints.deposit,
    "Error enviando su depósito. Inténtelo nuevamente más tarde.",
    (gt: GlacierTransaction) => {
      setPendingRequests([{
        amount: gt.amount,
        lastUpdate: Math.floor(gt.createdAt / 1000),
        status: "PENDING",
        type: "MINT",
        factory: {
          tokenAddress: native_token
        },
        transactions: [
          { id: gt.hash }
        ]
      }, ...pending_requests])
      setLastUpdated(last_updated + 1)
    }
  )

  const submitWithdraw = submit<WithdrawInfo, GlacierTransaction>(
    config.endpoints.withdraw,
    "Error enviando su petición de retiro. Inténtelo nuevamente más tarde.",
    (gt: GlacierTransaction) => {
      setErrors(["hola"])
      setRequests([{
        amount: gt.amount,
        lastUpdate: Math.floor(gt.createdAt / 1000),
        status: "PENDING",
        type: "BURN",
        factory: {
          tokenAddress: native_token
        },
        transactions: [
          { id: gt.hash }
        ]
      }, ...requests])
    }
  )

  const submitBankAccount = submit<BankAccountForm, BankAccount>(
    config.endpoints.bank_account,
    "Error creando nueva cuenta bancaria. Inténtelo nuevamente más tarde.",
    (ba: BankAccount) => setBankAccounts([ba, ...bank_accounts])
  )

  const [errors, setErrors] = useState<string[]>([])

  return (
    <GlacierContext.Provider value={{
      email,
      logged,
      loading,
      signin,
      logout,

      native_token,
      tokens,
      balances,
      rates,
      btc_rate,
      requests,
      pending_requests,

      submitDeposit,
      submitWithdraw,
      submitBankAccount,
      getNewAddress,

      aave_status,

      user_info,
      bank_accounts,
      deposit_address,
    }}>
      {children}
      {errors.map((e) => {
        (<Modal show={errors.length > 0}>
          {e}
        </Modal>)
      })}
    </GlacierContext.Provider>
  )
}
