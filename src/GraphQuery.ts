export const genQuery = (customerAddress: string) => {
  return `
query{
  pairs {
    id
    description
  }  
  factories {
    id
    tokenName
    tokenAddress
    tokenSymbol
    tokenDecimals
  }  
  priceCandles(
    orderBy: timestamp,
    orderDirection: desc,
    first:6
  ) {
    pair {
      id
      description
    }
    close
    timestamp
  }
  balances(where:
    {
      customer: "${customerAddress}"
    }) {
    factory {
      tokenAddress
      tokenDecimals
      tokenSymbol
    }
    balance
    inTransitIn
    inTransitOut
  }
  requests(
    where:{
      customer: "${customerAddress}"
    }
    orderBy: lastUpdate
    orderDirection: desc
  ){
    id
    amount
    lastUpdate
    requestHash
    status
    type
    factory{
      tokenAddress
      tokenDecimals
    }
    transactions(orderBy:timestamp, orderDirection:desc){
      id
      type
    }
  }
}
`
}

export const genQueryUpdate = (customerAddress: string, time: number) => {

  const q = `
query{
  balances(where:
    {
      customer: "${customerAddress.toLowerCase()}"
    }) {
    factory {
      tokenAddress
      tokenDecimals
      tokenSymbol
    }
    balance
    inTransitIn
    inTransitOut
  }
  priceCandles(
    orderBy: timestamp,
    orderDirection: desc,
    first:6
  ) {
    pair {
      id
    }
    close
  }
  requests(
    where:{
      customer: "${customerAddress.toLowerCase()}"
      lastUpdate_gt: ${time}
    }
    orderBy: lastUpdate
    orderDirection: desc
  ){
    amount
    lastUpdate
    requestHash
    status
    type
    factory{
      tokenAddress
      tokenDecimals
    }
    transactions(orderBy:timestamp, orderDirection:desc){
      id
      type
    }
  }
}
`
  return q
}
