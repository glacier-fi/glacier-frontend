
export interface GraphPair {
  description: string;
  id: string;
}

export interface GraphCandle {
  close: string;
  pair: GraphPair;
  timestamp: string;
}

export interface GraphFactory {
  id: string;
  tokenAddress: string;
  tokenDecimals: number;
  tokenName: string;
  tokenSymbol: string;
}

export interface GraphBalance {
  balance: string;
  factory: {
    tokenAddress: string;
  };
  inTransitIn: string;
  inTransitOut: string;
}

export interface GraphRequest {
  id: string;
  amount: string;
  factory: {
    tokenAddress: string;
    tokenDecimals: number;
  };
  lastUpdate: string;
  requestHash: string;
  status: string;
  transactions: [
    {
      id: string;
      type: string;
    }
  ];
  type: string;
}

export interface GraphResponse {
  pairs: GraphPair[];
  priceCandles: GraphCandle[];
  factories: GraphFactory[];
  balances: GraphBalance[];
  requests: GraphRequest[];
}

