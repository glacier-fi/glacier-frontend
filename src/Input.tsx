import { ChangeEventHandler, useEffect, useRef, useState } from "react"

export const Input = (props: {
  onChange?: (value: string | undefined) => void
  onInvalid?: ChangeEventHandler<HTMLInputElement>
  className?: string
  label: string
  name?: string
  disabled?: boolean
  type?: string
  step?: number
  max?: number
  min?: number
}) => {
  const [style, setStyle] = useState("")

  const input = useRef<HTMLInputElement>(null)

  const checkValidity = () => {
    if (input.current?.validity.valid)
      props.onChange && props.onChange(input.current.value)
    else props.onChange && props.onChange(undefined)

    if (
      (input.current?.validity.valid && input.current.value != "") ||
      (!input.current?.validity.valid && !input.current?.validity.valueMissing)
    ) {
      setStyle("is-valid")
    } else {
      setStyle("")
    }
  }

  useEffect(() => input.current?.focus())

  return (
    <div className="input-container">
      <input
        disabled={props.disabled}
        tabIndex={0}
        ref={input}
        onInvalid={props.onInvalid}
        onInput={() => checkValidity()}
        onChange={(i) =>
          i.target.validity.valid &&
          props.onChange &&
          props.onChange(i.target.value)
        }
        max={props.max}
        min={props.min}
        name={props.name}
        type={props.type}
        className={style + " " + props.className}
      />
      <label htmlFor={props.name}>{props.label}</label>
    </div>
  )
}

