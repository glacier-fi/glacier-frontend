import { BigNumber, FixedNumber } from "@ethersproject/bignumber"
import { useContext, useEffect, useState } from "react"
import clp from "./assets/images/currency/clp.svg"
import { Button } from "./Button"
import { GlacierContext } from "./GlacierContext"
import { Input } from "./Input"
import { Modal } from "./Modal"
import { Progress } from "./Progress"

enum Show {
  None = 0,
  Wallet,
  Lend,
}

const LendModal = () => {
  return (
    <>
      <div className="tab-form">
        <div className="tab-data">
          <ul>
            <li>
              <span className="legend">Tasa de Interés</span>
              <span className="val">1.5%</span>
            </li>
            <li>
              <span className="legend">Comisión</span>
              <span className="val">12%</span>
            </li>
          </ul>
        </div>
        <Input label="Prestar"
          type="number"
          step={1} />
        <div className="cont-notes">
          <ul className="notes">
            <li className="note">
              <i className="fa-light fa-circle-info" />
              <div className="val-note">
                <small>Las rentabilidades son variables y puede que no se mantengan en el futúro</small>
              </div>
            </li>
            <hr />
            <li className="note">
              <i className="fa-light fa-circle-info" />
              <div className="val-note">
                <small>La comisión está afecta a IVA y se calculará de forma prorrogativa</small>
              </div>
            </li>
            <hr />
            <li className="note">
              <i className="fa-light fa-circle-info" />
              <div className="val-note"><small>El protocolo no garantiza liquidez.</small></div>
            </li>
          </ul>
        </div>
      </div>
      <div className="tab-footer">
        <div className="tab-check">
          <input type="checkbox" />Estoy de acuerdo.
        </div>
        <Button
          className={"btn-primary btn-full"} >Prestar</Button>
      </div>
    </>
  )
}



const WalletModal = () => {


  return (
    <>
      <div className="tab-form">
        <div className="tab-data">
          <ul>
            <li>
              <span className="legend"> Liquidez disponible : </span>
              <span className="val">$ 7.012.232</span>
            </li>
          </ul>
        </div>
        <Input label="Monto"
          type="number"
          step={1} />
        <div className="tab-data">
          <ul>
            <li>
              <span className="legend"> Liquidez disponible : </span>
              <span className="val">$ 7.012.232</span>
            </li>
          </ul>
        </div>
        <div className="cont-notes">
          <ul className="notes">
            <li className="note">
              <i className="fa-light fa-circle-info" />
              <div className="val-note"><small>El protocolo no garantiza liquidez.</small></div>
            </li>
          </ul>
        </div>
        <hr />
      </div>
      <div className="tab-footer">
        <div className="tab-check">
          <input type="checkbox" />Estoy de acuerdo.
        </div>
        <Button
          className={"btn-primary btn-full"} >Enviar a mi billetera</Button>
      </div>
    </>
  )
}

export const Lend = () => {
  const [show_modal, setShowModal] = useState<Show>(Show.None)
  const { aave_status, tokens, native_token } = useContext(GlacierContext)

  const [clpAvailable, setCLPAvailable] = useState<string>("0")
  const [clpBorrowed, setCLPBorrowed] = useState<string>("0")
  const [clpTotal, setCLPTotal] = useState<string>("0")

  const [utilization, setUtilization] = useState<number>(0)
  const [rate, setRate] = useState<string>("0.0")

  useEffect(() => {
    if (!aave_status) return
    const clp = tokens.find(t => t.id == native_token)
    const fmt = new Intl.NumberFormat("es", clp?.format)
    const fmtRate = new Intl.NumberFormat("es", {minimumFractionDigits: 2, maximumFractionDigits: 2})
    console.log("status", aave_status)

    const available = FixedNumber.fromValue(BigNumber.from(aave_status?.GCLP.result[0].hex), clp?.decimals).toUnsafeFloat()
    const borrowed = FixedNumber.fromValue(BigNumber.from(aave_status?.GCLP.result[2].hex), clp?.decimals).toUnsafeFloat()
    const rateN = FixedNumber.fromValue(BigNumber.from(aave_status?.GCLP.result[3].hex), 23).toUnsafeFloat()

    setCLPAvailable(fmt.format(available))
    setCLPBorrowed(fmt.format(borrowed))
    setCLPTotal(fmt.format(available + borrowed))
    if (borrowed > 0) {
      setUtilization(((available + borrowed) / borrowed) * 100)
    }
    setRate(fmtRate.format(rateN))
  }, [aave_status])


  return (
    <>
      <div className="container-fluid">
        <div className="row">
          <div className="col-12 col-lg-4">
            <div className="card-glacier card-lend">
              <div className="card-header">
                <h1 className="title-box">Tasa de Interés</h1>
                <h2 className="label-box">Actual</h2>
                <div className="value-box">{rate}</div>
              </div>
              <div className="card-body">
                <ul className="data-list-box">
                  <li>
                    <em>Utilización de la reserva :</em>
                    <Progress value={utilization} />
                  </li>
                  <li>
                    <em>Total prestado :</em> <span>${clpBorrowed}</span>
                  </li>
                  <li>
                    <em>Liquidez Disponible :</em> <span>${clpAvailable}</span>
                  </li>
                  <li>
                    <em>Tamaño de la reserva :</em> <span>${clpTotal}</span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="col-12 col-lg-4">
            <div className="card-glacier card-lend">
              <div className="card-header">
                <h1 className="title-box">Has Prestado</h1>
                <h2 className="label-box">Balance</h2>
                <div className="value-box">
                  $ 0 <small>0</small>{" "}
                </div>
                <div className="button-bar">
                  <button className="btn btn-default" onClick={() => setShowModal(Show.Wallet)}>
                    Enviar a Mi Billetera
                  </button>
                </div>
              </div>
              <div className="card-body">
                <ul className="data-list-box">
                  <li>
                    <div className="logo-token">
                      <img src={clp} alt="" />
                    </div>
                    <span className="amount">
                      $ 0
                      <small>$ 0</small>
                    </span>
                  </li>
                </ul>
                <div className="button-bar" onClick={() => setShowModal(Show.Lend)}>
                  <button className="btn btn-primary">
                    Prestar
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-lg-4">
            <div className="card-glacier card-lend">
              <div className="message-card">
                <h1>Tus activos serán prestados a otros usuarios</h1>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Sollicitudin habitasse dignissim imperdiet urna dictumst
                  convallis est neque ante. Nunc montes, tortor lectus neque.
                  Nec orci pulvinar porta massa lectus. Fringilla morbi diam,.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Modal show={show_modal != Show.None} onClose={() => setShowModal(Show.None)} >
        <>
          <div className="tab-header">
            <button
              className={show_modal == Show.Lend ? "active" : ""}
              onClick={() => setShowModal(Show.Lend)}>Prestar</button>
            <button
              className={show_modal == Show.Wallet ? "active" : ""}
              onClick={() => setShowModal(Show.Wallet)}>Enviar a Mi Billetera</button>
          </div>
          <div className="tab-body">
            {show_modal == Show.Lend && (
              <LendModal />
            )}
            {show_modal == Show.Wallet && (
              <WalletModal />
            )}
          </div>
        </>
      </Modal>
    </>
  )
}
