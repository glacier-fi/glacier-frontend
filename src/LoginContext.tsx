import React, { FC, useEffect, useState } from "react"
import { Magic } from "magic-sdk"
import config from "./config"

interface ILoginContext {
  login_loading: boolean
  isLoggedIn: () => Promise<boolean>
  logged: boolean,
  logout: () => Promise<void>
  signin: (email: string) => Promise<string>
  email: string | undefined,
  getAuthToken: () => Promise<string | undefined>
}

const defaultState: ILoginContext = {
  login_loading: false,
  logged: false,
  isLoggedIn: async () => { return false },
  logout: async () => { return },
  signin: async () => { throw "Not implemented" },
  email: undefined,
  getAuthToken: async () => { throw "Not implemented" },
}

export const LoginContext =
  React.createContext<ILoginContext>(defaultState)

const m = new Magic(config.magic_link_key, { locale: "es" })
//m.preload()

export const LoginProvider: FC = ({ children }) => {
  const [loading, setLoading] = useState(false)
  const [email, setEmail] = useState<string | undefined>(undefined)
  const [logged, setLogged] = useState(false)

  const isLoggedIn = async () => {
    if (loading) return false
    return m.user.isLoggedIn()
  }

  const signin = async (e: string) => {
    setLoading(true)
    const key = await m.auth.loginWithMagicLink({ email: e })
    if (key != null) await getAuthToken()
    setEmail(e)
    setLoading(false)
    setLogged(true)
    if (key != null)
      window.localStorage.setItem("email", e)
    return key ?? e
  }

  const logout = async () => {
    setLoading(true)
    setLogged(false)
    setEmail(undefined)
    window.localStorage.clear()
    await m.user.logout()
    setLoading(false)
  }

  const checkLogin = () => {
    const now = (new Date()).getTime()

    if (config.dev_mode) {
      console.log("Dev mode")
      window.localStorage.setItem("email", "dev@glacier.fi")
      window.localStorage.setItem("token", "0000")
      window.localStorage.setItem("token_created", now.toString())
      return true
    }

    if (!window.localStorage.getItem("email")) {
      return false
    }

    const token = window.localStorage.getItem("token")
    const token_created = window.localStorage.getItem("token_created")

    if (token != null && token_created != null) {
      if ((+token_created) + (config.token_life_seconds - 120) > (now / 1000)) {
        return true
      }
    }

    return false
  }

  const getAuthToken = async () => {
    if (!checkLogin()) {
      const now = (new Date()).getTime()
      return m.user.getIdToken({ lifespan: config.token_life_seconds })
        .then(t => {
          window.localStorage.setItem("token", t)
          window.localStorage.setItem("token_created", (now / 1000).toString())
          return t
        })
    } else {
      const t = window.localStorage.getItem("token")
      if (t == null) throw "Error getting token"
      return t
    }
  }

  useEffect(() => {
    setLoading(true)

    if (checkLogin()) {
      setLogged(true)
      setLoading(false)
      const email = window.localStorage.getItem("email")
      if (email)
        setEmail(email)
    } else {
      m.user.isLoggedIn()
        .then(b => {
          if (b) {
            getAuthToken()
              .then(t => {
                if (!t) throw "Error getting token"
                return m.user.getMetadata()
              })
              .then((meta) => {
                if (meta.email) {
                  setEmail(meta.email)
                  window.localStorage.setItem("email", meta.email)
                }
                setLogged(true)
              })
          } else {
            setLogged(false)
            window.localStorage.removeItem("token")
            window.localStorage.removeItem("token_created")
          }
        })
        .catch(() => setLogged(false))
        .finally(() => setLoading(false))
    }

  }, [])

  return (
    <LoginContext.Provider value={{
      email,
      login_loading: loading,
      isLoggedIn,
      logged,
      logout,
      signin,
      getAuthToken,
    }}>
      {children}
    </LoginContext.Provider>
  )

}

