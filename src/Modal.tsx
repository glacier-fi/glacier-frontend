import {
  FC,
  useEffect,
  useRef,
  useState,
} from "react"

type Props = {
  showBack?: boolean,
  show: boolean,
  onBack?: () => void,
  onClose?: () => void,
  modal_full?: boolean
}

export const Modal: FC<Props> = (props): JSX.Element => {
  const [show, setShow] = useState(false)
  const overlay = useRef<HTMLDivElement>(null)

  useEffect(() => {
    setShow(props.show)
    const close = (e: { keyCode: number }) => {
      if (e.keyCode === 27) {
        props.onClose && props.onClose()
      }
    }
    window.addEventListener("keydown", close)
    return () => window.removeEventListener("keydown", close)
  })

  useEffect(() => {
    if (show) {
      overlay.current?.classList.add("visible")
      document.getElementById("body")?.classList.add("overflow-hidden")
    } else {
      document.getElementById("body")?.classList.remove("overflow-hidden")
    }

    return () =>
      document.getElementById("body")?.classList.remove("overflow-hidden")
  }, [show])

  return (
    <>
      {props.show && (
        <div
          ref={overlay}
          className="modal-overlay"
          onKeyDown={(e) => {
            if (e.key == "Escape") setShow(false)
          }}
        >
          <div className="modal">
            <div className="modal-header">
              <div className="modal-back">
                {props.showBack && (
                  <button
                    onClick={() => {
                      props.onBack && props.onBack()
                    }}
                  >
                    <i className="fa-solid fa-arrow-left" />
                  </button>
                )}
              </div>
              <div className="modal-close">
                <button
                  onClick={() => {
                    props.onClose && props.onClose()
                  }}
                >
                  <i className="fa-light fa-xmark" />
                </button>
              </div>
            </div>
            <div className={props.modal_full ? "" : "modal-body"}>{props.children}</div>
          </div>
        </div>
      )}
    </>
  )
}
