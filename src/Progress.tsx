
export const Progress = (props: { value: number }) => {
  return (
    <div className="progress-bar">
      <div className="progress-bar pb-line">
        <div
          className="progress-bar pb-progress"
          style={{ width: `${props.value}%` }}
        ></div>
      </div>
      <div className="progress-bar pb-percent">
        <small>{props.value} %</small>
      </div>
    </div>
  )
}
