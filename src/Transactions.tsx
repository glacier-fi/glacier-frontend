import { BigNumber, FixedNumber } from "ethers"
import { useContext, useEffect, useState } from "react"

import { GlacierContext, Request } from "./GlacierContext"

import moment from "moment"
import { Modal } from "./Modal"

moment.updateLocale("en", {
  relativeTime: {
    future: "en %s",
    past: "hace %s",
    s: "unos segundos",
    ss: "%d segundos",
    m: "un minuto",
    mm: "%d minutos",
    h: "una hora",
    hh: "%d horas",
    d: "un día",
    dd: "%d días",
    w: "una semana",
    ww: "%d semanas",
    M: "un mes",
    MM: "%d meses",
    y: "un año",
    yy: "%d años",
  },
})

type RequestProcessed = {
  last_updated: number
  type: string
  symbol: string
  amount: string
  amountF: number
  state: string
  hash: string
}

type Filters = { [key: string]: boolean }

const FiltersModal = (props: {
  onChange: (f: Filters) => void
  filters: Filters
}) => {
  return (
    <div className="tab-form">
      <div className="tab-title">Filtros</div>
      <div className="filter-list">
        <div className="filter-col">
          <strong>Tipo Transacción</strong>
          <div className="tab-data">
            <ul>
              <li>
                <label className="check-container">
                  <input
                    type="checkbox"
                    checked={props.filters["deposit"]}
                    onChange={(v) => {
                      props.filters.deposit = v.target.checked
                      props.onChange(props.filters)
                    }}
                  />
                  <span className="checkmark"></span>
                  Depósito
                </label>
              </li>
              <li>
                <label className="check-container">
                  <input
                    type="checkbox"
                    checked={props.filters["withdraw"]}
                    onChange={(v) => {
                      props.filters.withdraw = v.target.checked
                      props.onChange(props.filters)
                    }}
                  />
                  <span className="checkmark"></span>
                  Retiro
                </label>
              </li>
              <li>
                <label className="check-container">
                  <input
                    type="checkbox"
                    checked={props.filters["returned"]}
                    onChange={(v) => {
                      props.filters.returned = v.target.checked
                      props.onChange(props.filters)
                    }}
                  />
                  <span className="checkmark"></span>
                  Devolución
                </label>
              </li>
            </ul>
          </div>
        </div>
        <div className="filter-col">
          <strong>Estado</strong>
          <div className="tab-data">
            <ul>
              <li>
                <label className="check-container">
                  <input
                    type="checkbox"
                    checked={props.filters["confirmed"]}
                    onChange={(v) => {
                      props.filters.confirmed = v.target.checked
                      props.onChange(props.filters)
                    }}
                  />
                  <span className="checkmark"></span>
                  Confirmado
                </label>
              </li>
              <li>
                <label className="check-container">
                  <input
                    type="checkbox"
                    checked={props.filters["pending"]}
                    onChange={(v) => {
                      props.filters.pending = v.target.checked
                      props.onChange(props.filters)
                    }}
                  />
                  <span className="checkmark"></span>
                  Pendiente
                </label>
              </li>
              <li>
                <label className="check-container">
                  <input
                    type="checkbox"
                    checked={props.filters["rejected"]}
                    onChange={(v) => {
                      props.filters.rejected = v.target.checked
                      props.onChange(props.filters)
                    }}
                  />
                  <span className="checkmark"></span>
                  Rechazado
                </label>
              </li>
              <li>
                <label className="check-container">
                  <input
                    type="checkbox"
                    checked={props.filters["cancelled"]}
                    onChange={(v) => {
                      props.filters.cancelled = v.target.checked
                      props.onChange(props.filters)
                    }}
                  />
                  <span className="checkmark"></span>
                  Cancelado
                </label>
              </li>
            </ul>
          </div>
        </div>
        <div className="filter-col">
          <strong>Tipo Activo</strong>
          <div className="tab-data">
            <ul>
              <li>
                <label className="check-container">
                  <input
                    type="checkbox"
                    checked={props.filters["clp"]}
                    onChange={(v) => {
                      props.filters.clp = v.target.checked
                      props.onChange(props.filters)
                    }}
                  />
                  <span className="checkmark"></span>
                  CLP
                </label>
              </li>
              <li>
                <label className="check-container">
                  <input
                    type="checkbox"
                    checked={props.filters["eth"]}
                    onChange={(v) => {
                      props.filters.eth = v.target.checked
                      props.onChange(props.filters)
                    }}
                  />
                  <span className="checkmark"></span>
                  ETH
                </label>
              </li>
              <li>
                <label className="check-container">
                  <input
                    type="checkbox"
                    checked={props.filters["usd"]}
                    onChange={(v) => {
                      props.filters.usd = v.target.checked
                      props.onChange(props.filters)
                    }}
                  />
                  <span className="checkmark"></span>
                  USD
                </label>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  )
}

export const Transactions = () => {
  const { requests, pending_requests, tokens } = useContext(GlacierContext)
  const [requests_processed, setRequestProcessed] = useState<
  RequestProcessed[]
  >([])

  const [show_filters, setShowFilter] = useState(false)

  const [filters, setFilters] = useState<Filters>({
    clp: false,
    eth: false,
    usd: false,

    confirmed: false,
    pending: false,
    rejected: false,
    cancelled: false,

    deposit: false,
    withdraw: false,
    returned: false,
  })

  const [filters1, setFilters1] = useState<Filters>({
    clp: false,
    eth: false,
    usd: false,

    confirmed: false,
    pending: false,
    rejected: false,
    cancelled: false,

    deposit: false,
    withdraw: false,
    returned: false,
  })

  const names: { [key: string]: string } = {
    deposit: "Depósito",
    withdraw: "Retiro",
    returned: "Retorno",

    clp: "CLP",
    usd: "USD",
    eth: "ETH",

    confirmed: "Confirmado",
    rejected: "Rechazado",
    pending: "Pendiente",
    cancelled: "Cancelado",
  }

  const [sorted_by, setSortedBy] = useState("last_updated")

  const byLastUpdated = (desc: boolean) => {
    return (rhs: RequestProcessed, lhs: RequestProcessed) => {
      setOrder(desc)
      setSortedBy("last_updated")
      const r = rhs.last_updated < lhs.last_updated ? -1 : 1
      return desc ? r : r * -1
    }
  }

  const byType = (desc: boolean) => {
    return (rhs: RequestProcessed, lhs: RequestProcessed) => {
      setOrder(desc)
      setSortedBy("type")
      const r = rhs.type < lhs.type ? -1 : 1
      return desc ? r : r * -1
    }
  }

  const byAmount = (desc: boolean) => {
    return (rhs: RequestProcessed, lhs: RequestProcessed) => {
      setOrder(desc)
      setSortedBy("amount")
      const r = rhs.amountF < lhs.amountF ? -1 : 1
      return desc ? r : r * -1
    }
  }

  const byToken = (desc: boolean) => {
    return (rhs: RequestProcessed, lhs: RequestProcessed) => {
      setOrder(desc)
      setSortedBy("token")
      const r = rhs.symbol < lhs.symbol ? -1 : 1
      return desc ? r : r * -1
    }
  }

  const byState = (desc: boolean) => {
    return (rhs: RequestProcessed, lhs: RequestProcessed) => {
      setOrder(desc)
      setSortedBy("state")
      const r = rhs.state < lhs.state ? -1 : 1
      return desc ? r : r * -1
    }
  }

  const [sort, setSort] = useState<
  (r: RequestProcessed, l: RequestProcessed) => number
  >(() => byLastUpdated(false))
  const [order, setOrder] = useState(false)

  useEffect(() => {
    const formats: { [key: string]: Intl.NumberFormat } = {}

    tokens.forEach((t) => {
      formats[t.id] = new Intl.NumberFormat("es", t.format)
    })

    const requests_: RequestProcessed[] = []

    const filterFn = (acc: Request[], r: Request) => {
      console.log(r.type)

      if (filters.deposit || filters.withdraw || filters.returned) {
        if (
          !(
            (r.type == "MINT" && filters.deposit) ||
            (r.type == "BURN" && filters.withdraw) ||
            (r.type == "RETURN" && filters.returned)
          )
        )
          return acc
      }
      if (
        filters.confirmed ||
        filters.rejected ||
        filters.cancelled ||
        filters.pending
      ) {
        if (
          !(
            (r.status == "PENDING" && filters.pending) ||
            (r.status == "CANCELLED" && filters.cancelled) ||
            (r.status == "CONFIRMED" && filters.confirmed) ||
            (r.status == "REJECTED" && filters.rejected)
          )
        )
          return acc
      }
      if (filters.clp || filters.usd || filters.eth) {
        if (
          !(
            (r.factory.tokenAddress ==
              tokens.find((t) => t.symbol == "GCLP")?.id &&
              filters.clp) ||
            (r.factory.tokenAddress ==
              tokens.find((t) => t.symbol == "GUSD")?.id &&
              filters.usd) ||
            (r.factory.tokenAddress ==
              tokens.find((t) => t.symbol == "GETH")?.id &&
              filters.eth)
          )
        )
          return acc
      }

      acc.push(r)
      return acc
    }

    const filtered_pending = pending_requests.reduce(filterFn, [] as Request[])
    const filtered = requests.reduce(filterFn, [] as Request[])

    const rProcessed = (r: Request) => {
      const token = tokens.find(t => t.id == r.factory.tokenAddress)
      if(!token) return

      const amount = formats[r.factory.tokenAddress].format(
        FixedNumber.fromValue(BigNumber.from(r.amount), token.decimals).toUnsafeFloat()
      )

      const amountF = FixedNumber.fromValue(
        BigNumber.from(r.amount),
        8
      ).toUnsafeFloat()

      const state =
        r.status == "PENDING"
          ? "Pendiente"
          : r.status == "CONFIRMED" ? "Confirmado" : "Cancelado"

      const t = r.type == "MINT" ? "Depósito" : "Retiro"

      const symbol = token.symbol

      requests_.push({
        type: t,
        symbol: symbol,
        amount: amount,
        amountF: amountF,
        state: state,
        hash: r.transactions[0]?.id || "",
        last_updated: r.lastUpdate * 1000,
      })
    }

    filtered_pending.forEach(rProcessed)
    filtered.forEach(rProcessed)

    setRequestProcessed([...requests_.sort(sort)])
    setFilters1(filters)
  }, [requests, pending_requests, tokens, filters, sort])

  return (
    <div className="transactions">
      <div className="t-title">Transacciones</div>
      <div className="t-filter">
        <button
          className="btn btn-default"
          onClick={() => {
            setFilters1({ ...filters })
            setShowFilter(true)
          }}
        >
          <i className="fa-solid fa-filter" />
          &nbsp;&nbsp;Filtrar por
        </button>
        {Object.keys(filters).map((f) => {
          if (filters[f] as boolean)
            return (
              <button
                className="btn btn-default"
                onClick={() => {
                  filters[f] = false
                  setFilters({ ...filters })
                }}
              >
                {names[f]} &nbsp;&nbsp;
                <i className="fa-solid fa-xmark" />
              </button>
            )
        })}
      </div>
      <div className="t-body">
        <div className="t-header">
          <div className="t-h t-h-transactions">
            Transacciones
            <button onClick={() => setSort(() => byType(!order))}>
              {sorted_by == "type" ? (
                <>
                  {order ? (
                    <i className="fa-solid fa-sort-up" />
                  ) : (
                    <i className="fa-solid fa-sort-down" />
                  )}
                </>
              ) : (
                <i className="fa-solid fa-sort" />
              )}
            </button>
          </div>
          <div className="t-h t-h-right">
            Monto
            <button onClick={() => setSort(() => byAmount(!order))}>
              {sorted_by == "amount" ? (
                <>
                  {order ? (
                    <i className="fa-solid fa-sort-up" />
                  ) : (
                    <i className="fa-solid fa-sort-down" />
                  )}
                </>
              ) : (
                <i className="fa-solid fa-sort" />
              )}
            </button>
          </div>
          <div className="t-h t-h-right">
            Activo
            <button onClick={() => setSort(() => byToken(!order))}>
              {sorted_by == "token" ? (
                <>
                  {order ? (
                    <i className="fa-solid fa-sort-up" />
                  ) : (
                    <i className="fa-solid fa-sort-down" />
                  )}
                </>
              ) : (
                <i className="fa-solid fa-sort" />
              )}
            </button>
          </div>
          <div className="t-h t-h-right">
            Estado
            <button onClick={() => setSort(() => byState(!order))}>
              {sorted_by == "state" ? (
                <>
                  {order ? (
                    <i className="fa-solid fa-sort-up" />
                  ) : (
                    <i className="fa-solid fa-sort-down" />
                  )}
                </>
              ) : (
                <i className="fa-solid fa-sort" />
              )}
            </button>
          </div>
          <div className="t-h t-h-right">
            Fecha
            <button onClick={() => setSort(() => byLastUpdated(!order))}>
              {sorted_by == "last_updated" ? (
                <>
                  {order ? (
                    <i className="fa-solid fa-sort-up" />
                  ) : (
                    <i className="fa-solid fa-sort-down" />
                  )}
                </>
              ) : (
                <i className="fa-solid fa-sort" />
              )}
            </button>
          </div>
        </div>
        <div className="t-data">
          {requests_processed.length == 0 && (
            <div className="t-row">
              <small>No tienes transacciones</small>
            </div>
          )}
          {requests_processed.map((r, i) => {
            return (
              <div key={i}>
                <div className="t-row">
                  <div className="t-col transaction-type"> <small>Tipo</small> {r.type}</div>
                  <div className="t-col-right transaction-amount"> <small>Monto</small> {r.amount}</div>
                  <div className="t-col-right transaction-symbol"> <small>Moneda</small> {r.symbol}</div>
                  <div className="t-col-right transaction-hash">
                    <small>Estado</small>
                    {r.state}{" "}
                    {r.hash != "" && (
                      <a
                        href={`https://mumbai.polygonscan.com/tx/${r.hash}`}
                        target="_blank"
                      >
                        <i className="fa-solid fa-link" />
                      </a>
                    )}
                  </div>
                  <div className="t-col-right transaction-time">
                    <small>Fecha</small>
                    {moment(r.last_updated).fromNow()}
                  </div>
                </div>
              </div>
            )
          })}
        </div>
        <div className="t-footer">
          <div className="t-descr">
            {requests_processed.length > 0 ? (
              <small>
                {requests_processed.length} transacciones de{" "}
                {requests_processed.length}
              </small>
            ) : (
              <small>0 transacciones de 0</small>
            )}
          </div>
          {requests_processed.length > 0 && (
            <div className="t-pages">
              <button className="">
                <i className="fa-solid fa-arrow-left" />
              </button>
              <button className="">1</button>
              <button className="">
                <i className="fa-solid fa-arrow-right" />
              </button>
            </div>
          )}
          <div className="t-goto"></div>
        </div>
      </div>
      <Modal
        modal_full={true}
        show={show_filters}
        onClose={() => setShowFilter(false)}
      >
        <FiltersModal
          onChange={(f) => {
            setFilters({ ...f })
          }}
          filters={filters1}
        />
      </Modal>
    </div>
  )
}
