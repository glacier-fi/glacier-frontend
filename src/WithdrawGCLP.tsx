import { BigNumber, FixedNumber } from "ethers"
import { ReactNode, useContext, useEffect, useRef, useState } from "react"
import { banks, banksA } from "./banks"
import { Button } from "./Button"
import { BankAccount, GlacierContext, GlacierTransaction } from "./GlacierContext"
import { Input } from "./Input"

export const WithdrawGCLP = (
  props: {
    onCreate?: (d: GlacierTransaction) => void,
    onCreateAccount: () => void,
    onSelectAccount: () => void,
    bank_account?: BankAccount,
  }) => {
  const { email, balances, tokens, native_token, submitWithdraw } = useContext(GlacierContext)

  const [max_withdraw, setMaxWithdraw] = useState(0)
  const [min_withdraw] = useState(0)
  const [fmt, setFmt] = useState(new Intl.NumberFormat("es", { maximumFractionDigits: 0 }))
  const [amount, setAmount] = useState<BigNumber | undefined>(undefined)
  const [amount_id] = useState(0)

  const [secs, setSecs] = useState(5)
  const [gracePeriod, setGracePeriod] = useState(false)
  const [sending, setSending] = useState(false)

  useEffect(() => {
    const clp = tokens.find(t => t.id == native_token)

    if (!clp) {
      return
    }

    setFmt(new Intl.NumberFormat("es", clp.format))

    const balance = balances.find(b => b.factory.tokenAddress == native_token)

    if (!balance) {
      setMaxWithdraw(0)
      return
    }

    setMaxWithdraw(FixedNumber.fromValue(BigNumber.from(balance?.balance), 8).toUnsafeFloat())
    setFmt(new Intl.NumberFormat("es", clp.format))
  }, [balances, tokens])

  const timer = useRef(-1)
  const countdown_interval = useRef(-1)

  const cancelWithdraw = () => {
    window.clearTimeout(timer.current)
    window.clearInterval(countdown_interval.current)
    setSecs(5)
    setGracePeriod(false)
  }

  const sendWithdraw = () => {
    if (!props.bank_account || !amount) return
    setGracePeriod(true)

    let countdown = 5
    countdown_interval.current = window.setInterval(() => setSecs(--countdown), 1000)

    timer.current = window.setTimeout(() => {
      if (!props.bank_account) return
      setGracePeriod(false)
      setSending(true)
      submitWithdraw({
        bankAccountId: props.bank_account.id,
        amount: BigNumber.from(amount).toString(),
      })
        .then(d => {
          window.clearTimeout(timer.current)
          window.clearInterval(countdown_interval.current)
          props.onCreate && props.onCreate(d)
        })
        .catch((err) => {
          console.log(err)
          window.clearTimeout(timer.current)
          window.clearInterval(countdown_interval.current)
          setSending(false)
          setGracePeriod(false)
        })
    }, 5010)

    return () => {
      window.clearInterval(timer.current)
      window.clearInterval(countdown_interval.current)
    }
  }

  const formatAmount = () => {
    const fmt = new Intl.NumberFormat("es", { maximumFractionDigits: 0 })
    if (amount)
      return fmt.format(FixedNumber.fromValue(amount, 8).toUnsafeFloat())
    else
      return ""
  }

  const changeAmount = (a?: string) => {
    console.log(a)
    if (!a) {
      setAmount(undefined)
      return
    }

    setAmount(BigNumber.from(a).mul(10 ** 8))
  }

  return (
    <>
      {!gracePeriod && !sending && (
        <div className="tab-form">
          <div className="tab-data">
            <ul>
              <li>
                <span className="legend">Banco : </span>
                <span className="val">{props.bank_account && banksA[props.bank_account.bankId]}</span>
              </li>
              <li>
                <span className="legend">Cuenta :</span>
                <span className="val">{props.bank_account && props.bank_account.account}</span>
              </li>
              <li>
                <span className="legend">Correo :</span>
                <span className="val">{email}</span>
              </li>
            </ul>
          </div>
          <div className="account-buttons">
            <button className="link-small"
              onClick={() => props.onCreateAccount && props.onCreateAccount()}>
              <i className="fa-solid fa-plus" />
              Crear cuenta
            </button>
            <button className="link-small" onClick={() => props.onSelectAccount()}>
              <i className="fa-solid fa-arrow-right-arrow-left" />
              Usar otra cuenta
            </button>
          </div>
          <hr />
          <Input label="Monto"
            key={amount_id}
            type="number"
            step={1}
            min={min_withdraw}
            max={max_withdraw}
            disabled={sending || gracePeriod}
            onChange={a => changeAmount(a)} />
          <hr />
          <div className="tab-data">
            <ul>
              <li>
                <span className="legend">Tu balance:</span>
                <span className="val">$ {fmt.format(max_withdraw)}</span>
              </li>
            </ul>
          </div>
        </div>
      )}
      {(gracePeriod || sending) && (
        <div className="tab-form">
          <div className="tab-title">
            RETIRAS
          </div>
          <div className="tab-amount">
            {formatAmount()}
          </div>
          <div className="tab-check">
            <i className="fa-solid fa-check primary" />
            <small>La cuenta bancaria debe estar asociada a tu RUT.</small>
          </div>
          <div className="tab-check">
            <i className="fa-solid fa-check primary" />
            <small>La validación será realizada dentro de las próximas 24 horas.</small>
          </div>
        </div>
      )}
      <div>
        {!sending && !gracePeriod && (
          <Button
            className={`btn btn-full 
            ${(!amount || !props.bank_account) ? "btn-disabled" : "btn-primary"}`}
            onClick={() => sendWithdraw()}
            disabled={!amount || !props.bank_account}>Retirar</Button>
        )}
        {sending && !gracePeriod && (
          <Button
            className="btn btn-full btn-disabled loading">
            Enviando...
          </Button>
        )}
        {!sending && gracePeriod && (
          <Button
            className="btn btn-full btn-cancel"
            onClick={() => cancelWithdraw()}
          >
            Cancelar... {secs}
          </Button>
        )}
      </div>
    </>
  )
}

type Items = { id: string, label: ReactNode }

const Select = (
  props: {
    items: Items[],
    title?: string,
    onChange: (id: string) => void,
  }) => {

  return (
    <div className="tab-form tab-select">
      {props.title && (
        <div className="tab-title">{props.title}</div>
      )}
      <div className="select-body">
        {props.items.map((i, ndx) => {
          return (
            <Button
              className="btn-element-tab"
              onClick={() => props.onChange && props.onChange(i.id)}
              key={ndx}>
              <span className="element-tab-label">{i.label}</span>
              <i className="fa-light fa-check"></i>
            </Button>
          )
        })}
      </div>
    </div>
  )

}

export enum Body {
  Main = 1,
  CreateAccountForm,
  SelectAccount,
  SelectAccounType,
  SelectBank,
}

export const WithdrawGCLPMain = (
  props: {
    onCreate?: (d: GlacierTransaction) => void,
    setBody: (b: Body) => void,
    body: Body
  }) => {

  const { bank_accounts, submitBankAccount } = useContext(GlacierContext)
  const [accounts_select, setAccountSelect] = useState<Items[]>([])
  const [bank_account_id, setBankAccountId] = useState<string | undefined>(undefined)
  const [bank_id, setBankId] = useState<string | undefined>(undefined)
  const [account_type, setAccountType] = useState<string | undefined>(undefined)
  const [bank_account, setBankAccount] = useState<BankAccount | undefined>(undefined)
  const [loading, setLoading] = useState(false)

  const [account_types] = useState([
    { id: "VISTA", label: "Vista" },
    { id: "CORRIENTE", label: "Corriente" },
  ])

  const [banks_items] = useState(banks.map(b => { return { id: b.value, label: b.text } }))

  useEffect(() => {

    if (bank_accounts.length > 0) {
      setBankAccountId(bank_accounts[0].id)
    }

    setAccountSelect(bank_accounts.map(b => {
      const label = (
        <>
          <div className="name">
            {banksA[b.bankId]}
          </div>
          <div className="number-account">
            {b.account}
          </div>
        </>
      )

      return { id: b.id, label: label }
    }))
  }, [bank_accounts])

  useEffect(() => {
    setBankAccount(bank_accounts.find(ba => ba.id == bank_account_id))
  }, [bank_account_id])

  const onCreateAccount = (a: string) => {
    if (!a || a == "" || !bank_id || !account_type) return
    setLoading(true)
    submitBankAccount({
      account: a,
      bankId: bank_id,
      accountType: account_type,
    })
      .then(() => {
        props.setBody(Body.Main)
        setLoading(false)
        setAccountType(undefined)
        setBankId(undefined)
      })
  }

  return (
    <>
      {(props.body == Body.Main) &&
        (<WithdrawGCLP
          onCreateAccount={() => props.setBody(Body.CreateAccountForm)}
          onSelectAccount={() => props.setBody(Body.SelectAccount)}
          bank_account={bank_account}
          onCreate={props.onCreate} />)}
      {(props.body == Body.SelectAccount) && (
        <Select items={accounts_select} title="Seleccione cuenta bancaria"
          onChange={(ba) => { props.setBody(Body.Main); setBankAccountId(ba) }}
        />
      )}
      {(props.body == Body.CreateAccountForm) && (
        <CreateAccountForm
          loading={loading}
          bank_id={bank_id}
          account_type={account_type}
          onSelectBank={() => props.setBody(Body.SelectBank)}
          onSelectAccountType={() => props.setBody(Body.SelectAccounType)}
          onCreate={(a) => { onCreateAccount(a) }}
        />
      )}
      {(props.body == Body.SelectBank) && (
        <Select items={banks_items} title="Seleccione banco"
          onChange={(b) => { props.setBody(Body.CreateAccountForm); setBankId(b) }}
        />
      )}
      {(props.body == Body.SelectAccounType) && (
        <Select items={account_types} title="Seleccione tipo de cuenta"
          onChange={(t) => { props.setBody(Body.CreateAccountForm); setAccountType(t) }}
        />
      )}
    </>
  )
}

const CreateAccountForm = (
  props: {
    loading: boolean,
    bank_id?: string,
    account_type?: string,
    onSelectBank: () => void
    onSelectAccountType: () => void
    onCreate: (account: string) => void
  }) => {

  const [account, setAccount] = useState<string | undefined>(undefined)

  return (
    <>
      <div className="tab-form">
        <div className="tab-title">
          NUEVA CUENTA BANCARIA
        </div>
        <div className="create-account" >
          <div className="select" onClick={() => { if (!props.loading) props.onSelectBank() }} >
            <span>{props.bank_id ? banksA[props.bank_id] : "Seleccione banco..."}</span>
            <button><i className="fa-solid fa-angle-down" /></button>
          </div>
          <div className="select" onClick={() => { if (!props.loading) props.onSelectAccountType() }}>
            <span>{props.account_type ? props.account_type : "Seleccione tipo de cuenta..."}</span>
            <button><i className="fa-solid fa-angle-down" /></button>
          </div>
          <Input
            disabled={props.loading}
            type="number" min={1000}
            onChange={(a) => setAccount(a)} label="Número de cuenta" />
        </div>
      </div>
      <div>
        <Button
          className={!account || !props.bank_id || !props.account_type
            ? "btn-full btn-disabled" : "btn-full btn-primary"}
          loading={props.loading}
          onClick={() => { account && props.onCreate(account) }}
          disabled={!account || !props.bank_id || !props.account_type}>
          {props.loading ? "Creando..." : "Crear cuenta"}
        </Button>
      </div>
    </>
  )
}
