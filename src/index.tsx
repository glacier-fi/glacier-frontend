import React, { FormEvent, useContext, useEffect, useState } from "react"

import ReactDOM from "react-dom"
import "./index.css"
// import App from "./App"
import reportWebVitals from "./reportWebVitals"

import logo from "./assets/svg/logo.svg"
import logo_day from "./assets/svg/logo-light.svg"

import clp from "./assets/images/currency/clp.svg"
import eth from "./assets/images/currency/eth.svg"
import usd from "./assets/images/currency/usdc.svg"
import btc from "./assets/images/currency/btc.svg"

import {
  BrowserRouter,
  Link,
  Route,
  Routes,
  useLocation,
} from "react-router-dom"
import { LoginProvider } from "./LoginContext"
import {
  GlacierProvider,
  GlacierContext,
  Balance,
  GlacierTransaction,
} from "./GlacierContext"
import { BigNumber, FixedNumber } from "ethers"

import QR from "./qr"
import { Transactions } from "./Transactions"
import { Lend } from "./Lend"
import { Borrow } from "./Borrow"
import { Modal } from "./Modal"
import { DepositGCLP } from "./DepositGCLP"
import { Body, WithdrawGCLPMain } from "./WithdrawGCLP"
import { Input } from "./Input"
import { Button } from "./Button"
import { Progress } from "./Progress"

const TokenBalance = (props: {
  img: string
  symbol: string
  balance?: TokenBalanceData
  perc?: number
  soon?: boolean
  onDeposit?: () => void
  onWithdraw?: () => void
}) => {
  const { logged } = useContext(GlacierContext)

  return (
    <div className={`token-balance content ${props.soon && "soon"}`}>
      <div className="logo-token">
        <img src={props.img} />
        {props.symbol}
      </div>

      <div className="amount">
        {props.balance ? props.balance?.amount : "0"}
        <small>$ {props.balance ? props.balance?.amount_rate : "0"}</small>
      </div>

      <div className="token-percent">
        <Progress value={props.perc || 0} />
      </div>

      <div className="token-actions">
        {props.onWithdraw && (
          <button
            className={`btn ${!props.onWithdraw || !logged ? "btn-disabled" : "btn-default"}`}
            onClick={() => {
              if (props.onWithdraw) props.onWithdraw()
            }}
          >
            Retirar
          </button>
        )}
        {!props.onWithdraw && <div className="btn-bar"> — </div>}
        {props.onDeposit && (
          <button
            className={`btn ${!props.onDeposit || !logged ? "btn-disabled" : "btn-primary"}`}
            disabled={!props.onDeposit || !logged}
            onClick={() => {
              if (props.onDeposit) props.onDeposit()
            }}
          >
            Depositar
          </button>
        )}
        {!props.onDeposit && <div className="btn-bar"> — </div>}
      </div>
    </div>
  )
}

enum ModalTab {
  D_GCLP = 1,
  W_GCLP,
  D_GETH,
  W_GETH,
  D_GUSD,
  W_GUSD,
}

const BankTransactions = (props: {
  show?: ModalTab
  onCreate?: (g: GlacierTransaction) => void
  onClose: () => void
}) => {
  const { loading } = useContext(GlacierContext)
  const [show, setShow] = useState(props.show)
  const [body, setBody] = useState(Body.Main)
  const [show_back, setShowBack] = useState(false)

  const onBack = () => {
    if (body == Body.SelectAccount || body == Body.CreateAccountForm) {
      setShowBack(false)
      setBody(Body.Main)
    } else if (body == Body.SelectAccounType || body == Body.SelectBank) {
      setBody(Body.CreateAccountForm)
    }
  }

  return (
    <>
      <Modal
        show={true}
        onClose={props.onClose}
        showBack={show == ModalTab.W_GCLP && body != Body.Main}
        onBack={onBack}
      >
        <div className="tab-header">
          <button
            className={show == ModalTab.D_GCLP ? "active" : ""}
            disabled={loading}
            onClick={() => setShow(ModalTab.D_GCLP)}
          >
            Depositar
          </button>
          <button
            className={show == ModalTab.W_GCLP ? "active" : ""}
            disabled={loading}
            onClick={() => setShow(ModalTab.W_GCLP)}
          >
            Retirar
          </button>
        </div>
        <div className="tab-body">
          {show == ModalTab.D_GCLP && (
            <DepositGCLP
              onCreate={(t) => props.onCreate && props.onCreate(t)}
            />
          )}
          {show == ModalTab.W_GCLP && (
            <WithdrawGCLPMain
              onCreate={(t) => props.onCreate && props.onCreate(t)}
              body={body}
              setBody={setBody}
            />
          )}
        </div>
      </Modal>
    </>
  )
}

const CryptoTransactionsGUSD = (props: {
  show?: ModalTab
  onCreate?: (g: GlacierTransaction) => void
}) => {
  const [show, setShow] = useState(props.show)

  return (
    <>
      <div className="tab-header">
        <button
          className={show == ModalTab.W_GUSD ? "active" : ""}
          onClick={() => setShow(ModalTab.W_GUSD)}
        >
          Retirar
        </button>
      </div>
      <div className="tab-body">
        {show == ModalTab.W_GUSD && (
          <WithdrawGUSD onCreate={(t) => props.onCreate && props.onCreate(t)} />
        )}
        {show == ModalTab.D_GUSD && <></>}
      </div>
    </>
  )
}
const CryptoTransactionsGETH = (props: {
  show?: ModalTab
  onCreate?: (g: GlacierTransaction) => void
}) => {
  const [show, setShow] = useState(props.show)

  return (
    <>
      <div className="tab-header">
        <button
          className={show == ModalTab.D_GETH ? "active" : ""}
          onClick={() => setShow(ModalTab.D_GETH)}
        >
          Depositar
        </button>
        <button
          className={show == ModalTab.W_GETH ? "active" : ""}
          onClick={() => setShow(ModalTab.W_GETH)}
        >
          Retirar
        </button>
      </div>
      <div className="tab-body">
        {show == ModalTab.D_GETH && <DepositGETH />}
        {show == ModalTab.W_GETH && (
          <WithdrawGETH onCreate={(t) => props.onCreate && props.onCreate(t)} />
        )}
        {show == ModalTab.W_GUSD && (
          <WithdrawGUSD onCreate={(t) => props.onCreate && props.onCreate(t)} />
        )}
      </div>
    </>
  )
}

export const DepositGETH = () => {
  const { deposit_address, getNewAddress, user_info } =
    useContext(GlacierContext)

  const [loading, setLoading] = useState(false)

  const getAddress = () => {
    setLoading(true)
    if (user_info) {
      getNewAddress({ id: user_info.id }).then(() => setLoading(false))
    }
  }

  return (
    <>
      <div className="tab-data">
        <ul>
          <li>
            <span className="legend">Token : </span>
            <span className="val">ETH - Ethereum</span>
          </li>
          <li>
            <span className="legend">Red :</span>
            <span className="val">ETH - Ethereum (ERC20)</span>
          </li>
        </ul>
      </div>
      <div className="tab-form">
        {deposit_address && (
          <>
            <h4>Dirección para el depósito:</h4>
            <div className="address-bar">
              {deposit_address || ""}
              <button className="btn-address">
                {" "}
                <i className="fa-solid fa-copy" onClick={() => navigator.clipboard.writeText(deposit_address)} />
              </button>
            </div>
            <div className="qr">
              <QR to={deposit_address} />
            </div>
            <hr />
            <div className="cont-notes">
              <ul className="notes">
                <li className="note">
                  <i className="fa-light fa-circle-info" />
                  <div className="val-note">
                    Envía <em>solo ETH</em> a esta dirección de depósito
                  </div>
                </li>

                <li className="note">
                  <i className="fa-light fa-circle-info" />
                  <div className="val-note">
                    Asegúrate que la red es <em>Ethereum (ERC20)</em>
                  </div>
                </li>

                <li className="note">
                  <i className="fa-light fa-circle-info" />
                  <div className="val-note">
                    Solo podrás pedir prestado si tu colateral vale al menos{" "}
                    <em>$1.000.000 CLP</em>
                  </div>
                </li>
              </ul>
            </div>
          </>
        )}
        {!deposit_address && (
          <small>
            Aún no tienes una dirección de depósito. Haz click para obtener una.
          </small>
        )}
      </div>

      <div>
        {!deposit_address && (
          <Button
            className={`btn-primary btn-full ${loading ? "loading" : ""}`}
            disabled={loading}
            onClick={() => getAddress()}
          >
            Obtener dirección de deposito
          </Button>
        )}
      </div>
    </>
  )
}

export const WithdrawGETH = (props: {
  show?: ModalTab
  onCreate?: (g: GlacierTransaction) => void
}) => {
  return (
    <>
      <div className="tab-data">
        <ul>
          <li>
            <span className="legend">Token : </span>
            <span className="val">ETH - Ethereum</span>
          </li>
          <li>
            <span className="legend">Red :</span>
            <span className="val">ETH - Ethereum (ERC20)</span>
          </li>
        </ul>
      </div>
      <div className="tab-form">
        <Input label="Dirección" disabled={true} />
        <Input label="Monto a retirar" disabled={true} />
        <hr />
        <div className="tab-data">
          <ul>
            <li>
              <span className="legend">Tu saldo:</span>
              <span className="val">0,79823</span>
            </li>
            <li>
              <span className="legend">Recivirás:</span>
              <span className="val"> 100000000</span>
            </li>
            <li>
              <span className="legend">Comisión de red</span>
              <span className="val">0,01</span>
            </li>
          </ul>
        </div>
      </div>
      <div>
        <Button className="btn-disabled btn-full" disabled={true}>
          Retirar
        </Button>
      </div>
    </>
  )
}

export const WithdrawGUSD = (props: {
  show?: ModalTab
  onCreate?: (g: GlacierTransaction) => void
}) => {
  return (
    <>
      <div className="tab-data">
        <ul>
          <li>
            <span className="legend">Token : </span>
            <span className="val">USDC - Ethereum</span>
          </li>
          <li>
            <span className="legend">Red :</span>
            <span className="val">ETH - Ethereum (ERC20)</span>
          </li>
        </ul>
      </div>
      <div className="tab-form">
        <Input label="Dirección" disabled={true} />
        <Input label="Monto a retirar" disabled={true} />
        <hr />
        <div className="tab-data">
          <ul>
            <li>
              <span className="legend">Tu saldo:</span>
              <span className="val">0,00</span>
            </li>
            <li>
              <span className="legend">Recivirás:</span>
              <span className="val"> 0,00</span>
            </li>
            <li>
              <span className="legend">Comisión de red</span>
              <span className="val">0,01</span>
            </li>
          </ul>
        </div>
      </div>
      <div>
        <Button className="btn-disabled btn-full" disabled={true}>
          Retirar
        </Button>
      </div>
    </>
  )
}

const Totales = (props: {
  total: string
  totalIn: string
  totalOut: string
}) => {
  return (
    <>
      <div className="total content">
        <label>
          Balance &nbsp;
          <button>
            <i className="fa-solid fa-eye" />
          </button>
        </label>
        <span className="tot-amount">$ {props.total}</span>
      </div>
      <div className="inout content">
        <div className="total-in">
          <div>
            <small>
              GCLP Entrante&nbsp;
              <i className="fa-solid fa-arrow-up-right green" />
            </small>
          </div>
          <div>
            <div>
              <div className="amount">
                $ {props.totalIn}
                <small>$ {props.totalIn}</small>
              </div>
            </div>
          </div>
        </div>
        <div className="total-out">
          <div>
            <small>
              GCLP Saliente&nbsp;
              <i className="fa-solid fa-arrow-down-right red" />
            </small>
          </div>
          <div>
            <div className="amount">
              $ {props.totalOut}
              <small>$ {props.totalOut}</small>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const LoginForm = (props: { onDone?: (email: string) => void }) => {
  const { loading, signin } = useContext(GlacierContext)

  const [email, setEmail] = useState<string | undefined>(undefined)
  const [message, setMessage] = useState<string>("Entrar")
  const [disabled] = useState(false)

  const login = (e: FormEvent<HTMLFormElement>) => {
    if (!email) return
    setMessage("Ingresando...")
    signin(email).then(() => {
      setMessage("Entrar")
      props.onDone && props.onDone(email)
    })
    e.preventDefault()
  }

  return (
    <>
      <div className="login-form">
        <div className="login-form-header">Ingreso</div>
        <form onSubmit={(e) => login(e)}>
          <div className="login-form-body">
            <Input
              label="Correo electrónico"
              disabled={disabled || loading}
              type="email"
              onChange={(value) => {
                setEmail(value)
              }}
            />
          </div>
          <div className="login-form-button">
            <Button
              className={`btn btn-primary ${loading && "loading"}`}
              disabled={!email}
            >
              {message}
            </Button>
          </div>
        </form>
      </div>
    </>
  )
}

const Header = () => {
  const { loading, logged, email, logout } = useContext(GlacierContext)
  const [login_modal, setShowModal] = useState(false)
  const [theme, setTheme] = useState("dark")

  const clickLogin = () => {
    if (logged) {
      logout()
    } else {
      setShowModal(true)
    }
  }

  const changeTheme = () => {
    const theme = window.localStorage.getItem("theme")
    if (!theme || theme == "dark") {
      document.getElementById("body")?.classList.remove("dark")
      document.getElementById("body")?.classList.add("light")
      document.getElementById("body")?.setAttribute("data-theme", "light")
      window.localStorage.setItem("theme", "light")
      setTheme("light")
    } else {
      document.getElementById("body")?.classList.remove("light")
      document.getElementById("body")?.classList.add("dark")
      document.getElementById("body")?.setAttribute("data-theme", "dark")
      window.localStorage.setItem("theme", "dark")
      setTheme("dark")
    }
  }

  useEffect(() => {
    const theme = window.localStorage.getItem("theme")
    if (!theme || theme == "dark") {
      setTheme("dark")
    } else {
      setTheme("light")
    }
  }, [])

  const location = useLocation()
  const rootStyle = location.pathname == "/" ? "active" : ""
  const lendStyle = location.pathname == "/lend" ? "active" : ""
  const borrowStyle = location.pathname == "/borrow" ? "active" : ""

  return (
    <>
      <div className="header">
        <div className="logo">
          {theme == "dark" && <img src={logo} />}
          {theme == "light" && <img src={logo_day} />}
          <div className="menu-icon">
            <i className="fa-solid fa-bars"></i>
          </div>
        </div>
        <div className="container-menu">
          <div className="main-menu">
            <Link to="/" className={`${rootStyle}`}>
              Mi Billetera
            </Link>
            <Link to="/lend" className={`${lendStyle}`}>
              Prestar
            </Link>
            <Link to="/borrow" className={`${borrowStyle}`}>
              Pedir Prestado
            </Link>
          </div>

          <div className="switch">
            <div id="switch" className="switch"></div>
          </div>

          <div className="button-login">
            {/* Switch Theme */}
            <button
              className="theme-toggle"
              id="theme-toggle"
              title="Toggles light & dark"
              aria-label="auto"
              aria-live="polite"
              onClick={() => changeTheme()}
            >
              <svg
                className="sun-and-moon"
                aria-hidden="true"
                width="24"
                height="24"
                viewBox="0 0 24 24"
              >
                <mask className="moon" id="moon-mask">
                  <rect x="0" y="0" width="100%" height="100%" fill="white" />
                  <circle cx="24" cy="10" r="6" fill="black" />
                </mask>
                <circle
                  className="sun"
                  cx="12"
                  cy="12"
                  r="6"
                  mask="url(#moon-mask)"
                  fill="currentColor"
                />
                <g className="sun-beams" stroke="currentColor">
                  <line x1="12" y1="1" x2="12" y2="3" />
                  <line x1="12" y1="21" x2="12" y2="23" />
                  <line x1="4.22" y1="4.22" x2="5.64" y2="5.64" />
                  <line x1="18.36" y1="18.36" x2="19.78" y2="19.78" />
                  <line x1="1" y1="12" x2="3" y2="12" />
                  <line x1="21" y1="12" x2="23" y2="12" />
                  <line x1="4.22" y1="19.78" x2="5.64" y2="18.36" />
                  <line x1="18.36" y1="5.64" x2="19.78" y2="4.22" />
                </g>
              </svg>
            </button>
            {/* Switch Theme */}
            <Button
              className="btn btn-primary btn-login"
              loading={loading}
              disabled={loading}
              onClick={() => clickLogin()}
            >
              {!logged ? "Ingresar" : email}
            </Button>

            {/*
            <div className="user-initials">
              
              <div className="menu-dropdown">
                <div className="header-menu">ma***@***.fi</div>
                <div className="no-verificado"><i className="fal fa-exclamation-triangle"></i> No Verificado</div>
                <ul className="menu-options">
                  <li>
                    <i className="fal fa-address-card"></i> Verificación de
                    Identidad <span className="warning"></span>
                  </li>
                  <li>
                    <i className="fal fa-shield-check"></i> Seguridad
                  </li>
                  <li>
                    <i className="fal fa-file-alt"></i> Certificados
                  </li>
                  <li>
                    <i className="fal fa-credit-card"></i> Cuentas Corrientes
                  </li>
                </ul>
                <div className="footer-menu">
                  <i className="far fa-sign-out"></i> Cerrar Sesión
                </div>
              </div>
            </div>
              */}
          </div>
        </div>
      </div>
      {login_modal && (
        <Modal show={login_modal} onClose={() => setShowModal(false)}>
          <LoginForm onDone={() => setShowModal(false)} />
        </Modal>
      )}
    </>
  )
}

type TokenBalanceData = { b: Balance; amount: string; amount_rate: string }

const Main = () => {
  const { logged, tokens, native_token, balances, rates } =
    useContext(GlacierContext)

  const [total, setTotal] = useState("0")
  const [totalIn, setIn] = useState("0")
  const [totalOut, setOut] = useState("0")

  const [base_rate, setBaseRate] = useState(native_token)

  const [token_balances, setTokenBalances] = useState<{
    [key: string]: TokenBalanceData
  }>({})

  useEffect(() => {
    const token_balances: { [key: string]: TokenBalanceData } = {}
    const base_token = tokens.find((t) => t.id.toLowerCase() == base_rate)
    const rateD = rates.find(
      (r) => r.pair.id.toLowerCase() == base_token?.ratio
    )
    let rate = 1.0

    if (rateD) {
      rate = FixedNumber.fromValue(
        BigNumber.from(rateD.close),
        8
      ).toUnsafeFloat()
    }

    balances.forEach((b) => {
      const token = tokens.find((t) => t.id == b.factory.tokenAddress)
      if (!token) return

      const token_rate_bn = rates.find((r) => r.pair.id == token.ratio)?.close

      const token_rate = token_rate_bn
        ? FixedNumber.fromValue(
          BigNumber.from(token_rate_bn),
          8
        ).toUnsafeFloat()
        : 1.0

      const format = new Intl.NumberFormat("es", token.format)
      const format_s = new Intl.NumberFormat(
        "es",
        base_token?.format || token.format
      )

      const total = FixedNumber.fromValue(
        BigNumber.from(b.balance),
        token.decimals
      ).toUnsafeFloat()

      const b_in = FixedNumber.fromValue(
        BigNumber.from(b.inTransitIn),
        token.decimals
      ).toUnsafeFloat()

      const b_out = FixedNumber.fromValue(
        BigNumber.from(b.inTransitOut),
        token.decimals
      ).toUnsafeFloat()

      token_balances[token.symbol] = {
        b: b,
        amount: format.format(total),
        amount_rate: format_s.format((total * token_rate) / rate),
      }

      if (b.factory.tokenAddress == native_token) {
        setTotal(format.format(total))
        setIn(format.format(b_in))
        setOut(format.format(b_out))
      }
    })

    setTokenBalances(token_balances)
  }, [balances, tokens, native_token, base_rate])

  const [show, setShow] = useState<ModalTab | undefined>(undefined)

  return (
    <>
      <div className="main">
        <div className="totales">
          <Totales total={total} totalIn={totalIn} totalOut={totalOut} />
        </div>
        <div className="token">
          <TokenBalance
            img={clp}
            symbol="GCLP"
            balance={token_balances["GCLP"]}
            onDeposit={() => setShow(ModalTab.D_GCLP)}
            onWithdraw={() => setShow(ModalTab.W_GCLP)}
          />
          <TokenBalance
            img={eth}
            symbol="GETH"
            onDeposit={() => setShow(ModalTab.D_GETH)}
            onWithdraw={() => setShow(ModalTab.W_GETH)}
            balance={token_balances["GETH"]}
          />
          <TokenBalance
            img={usd}
            symbol="USDC"
            onWithdraw={() => setShow(ModalTab.W_GUSD)}
            balance={token_balances["GUSD"]}
          />
          <TokenBalance img={btc} symbol="BTC" soon={true} />
        </div>
      </div>
      {logged && <Transactions />}
      {(show == ModalTab.D_GCLP || show == ModalTab.W_GCLP) && (
        <BankTransactions
          show={show}
          onClose={() => setShow(undefined)}
          onCreate={() => setShow(undefined)}
        />
      )}
      {(show == ModalTab.D_GETH || show == ModalTab.W_GETH) && (
        <Modal
          show={true}
          onClose={() => {
            setShow(undefined)
          }}
        >
          <CryptoTransactionsGETH
            show={show}
            onCreate={() => setShow(undefined)}
          />
        </Modal>
      )}
      {show == ModalTab.W_GUSD && (
        <Modal
          show={true}
          onClose={() => {
            setShow(undefined)
          }}
        >
          <CryptoTransactionsGUSD
            show={show}
            onCreate={() => setShow(undefined)}
          />
        </Modal>
      )}
    </>
  )
}

const App = () => {
  return (
    <>
      <Header />
      <Routes>
        <Route path="/" element={<Main />} />
        <Route path="/lend" element={<Lend />} />
        <Route path="/borrow" element={<Borrow />} />
      </Routes>
    </>
  )
}

const theme = window.localStorage.getItem("theme")

if (!theme || theme == "dark") {
  document.getElementById("body")?.classList.remove("light")
  document.getElementById("body")?.classList.add("dark")
  document.getElementById("body")?.setAttribute("data-theme", "dark")
  window.localStorage.setItem("theme", "dark")
} else {
  document.getElementById("body")?.classList.remove("dark")
  document.getElementById("body")?.classList.add("light")
  document.getElementById("body")?.setAttribute("data-theme", "light")
  window.localStorage.setItem("theme", "light")
}

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <LoginProvider>
        <GlacierProvider>
          <App />
        </GlacierProvider>
      </LoginProvider>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById("root")
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
